// Request history
package history

import (
	"errors"
	"fmt"
	"strconv"
	"sync"
	"teamspot/itvtest/fetcher"
	"time"
)

type Item struct {
	CreatedAt time.Time
	Response  *fetcher.FetchResponse
}

type Storage struct {
	mu             sync.RWMutex
	itemsIdToItem  map[string]*Item // index for searching by ID
	itemsIdToIndex map[string]int
	items          []*Item
}

// Get all items
func (h *Storage) GetAll(limitParam, offsetParam string) (SortedItems, error) {
	h.mu.RLock()
	defer h.mu.RUnlock()

	limit, _ := strconv.Atoi(limitParam)
	offset, _ := strconv.Atoi(offsetParam)

	startIndex := 0
	endIndex := len(h.items)

	if offset > 0 {
		startIndex = offset
	}
	if limit > 0 {
		endIndex = startIndex + limit
	}

	if startIndex >= len(h.items) {
		return nil, nil
	}
	if endIndex > len(h.items) {
		endIndex = len(h.items)
	}

	return h.items[startIndex:endIndex], nil
}

// Get item by Response ID
func (h *Storage) Get(id string) (*Item, error) {
	h.mu.RLock()
	defer h.mu.RUnlock()

	if item, exist := h.itemsIdToItem[id]; exist {
		return item, nil
	}
	return nil, errors.New("not found by id: " + id)
}

// Add entry to history storage
func (h *Storage) Add(response *fetcher.FetchResponse) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	if h.itemsIdToItem == nil {
		h.itemsIdToItem = make(map[string]*Item)
	}
	if h.itemsIdToIndex == nil {
		h.itemsIdToIndex = make(map[string]int)
	}

	item := &Item{CreatedAt: time.Now(), Response: response}
	h.items = append(h.items, item)
	h.itemsIdToItem[response.Id] = item
	h.itemsIdToIndex[response.Id] = len(h.items) - 1
	return nil
}

// Remove from storage by ID
func (h *Storage) Remove(id string) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	if index, exist := h.itemsIdToIndex[id]; exist {
		// removing element
		h.items = append(h.items[:index], h.items[index+1:]...)

		// removing from Index map
		delete(h.itemsIdToIndex, id)

		// removing from Id map
		delete(h.itemsIdToItem, id)
		return nil
	}
	return fmt.Errorf("not found by id: %s", id)
}

// ----------------
type SortedItems []*Item

func (items SortedItems) Len() int {
	return len(items)
}

func (items SortedItems) Less(i, j int) bool {
	return items[i].CreatedAt.Before(items[j].CreatedAt)
}

func (items SortedItems) Swap(i, j int) {
	items[i], items[j] = items[j], items[i]
}

// ----------------
