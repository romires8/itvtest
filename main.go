package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"teamspot/itvtest/api"
	"teamspot/itvtest/fetcher"
	"teamspot/itvtest/history"
)

var storage = &history.Storage{}

func main() {

	serverAddress := ":8090"

	// Index
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Use:\n /fetch/ \n /history/list/?offcet=X&limit=Y \n /history/remove/")
	})

	// Fetch request
	http.HandleFunc("/fetch/", func(w http.ResponseWriter, r *http.Request) {
		response := &api.Response{}
		fetchRequest := &fetcher.FetchRequest{}

		// Read request from client
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.Error = fmt.Sprintf("read request: %s", err)
			SendResponse(w, response)
			return
		}

		// Check request empty
		if len(data) == 0 {
			response.Error = fmt.Sprintf("request is empty")
			SendResponse(w, response)
			return
		}

		// Parse request
		err = json.Unmarshal(data, fetchRequest)
		if err != nil {
			response.Error = fmt.Sprintf("request parsing: %s", err)
			SendResponse(w, response)
			return
		}

		// Perform fetch request
		fetchResponse, err := fetcher.Fetch(fetchRequest)
		if err != nil {
			response.Error = fmt.Sprintf("performing fetch: %s", err)
			SendResponse(w, response)
			return
		}

		response.Data = fetchResponse
		SendResponse(w, response)

		// Adding to history (without body)
		fetchResponse.Body = nil
		err = storage.Add(fetchResponse)
		if err != nil {
			log.Fatal(fmt.Sprintf("adding to history: %s", err))
		}
	})

	// Show history
	http.HandleFunc("/history/list/", func(w http.ResponseWriter, r *http.Request) {
		response := &api.Response{}

		items, err := storage.GetAll(
			r.URL.Query().Get("limit"),
			r.URL.Query().Get("offset"),
		)

		// Уже отсортированы в порядке добавления
		// sort.Sort(items)

		if err != nil {
			response.Error = fmt.Sprintf("get history: %s", err)
			SendResponse(w, response)
			return
		}
		response.Data = items
		SendResponse(w, response)
	})

	// Delete history entry
	http.HandleFunc("/history/remove/", func(w http.ResponseWriter, r *http.Request) {

		response := &api.Response{}
		// Read request from client
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.Error = fmt.Sprintf("read request: %s", err)
			SendResponse(w, response)
			return
		}
		// ID number in body of request
		id := string(data)
		err = storage.Remove(id)
		if err != nil {
			response.Error = fmt.Sprintf("removing entry: %s", err)
			SendResponse(w, response)
			return
		}

		response.Data = id
		SendResponse(w, response)
	})

	fmt.Printf("Server running on %s ...\n", serverAddress)
	log.Fatal(http.ListenAndServe(serverAddress, nil))
}

func SendResponse(w io.Writer, response interface{}) {
	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		log.Fatal(err)
	}
}
