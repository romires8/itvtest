package fetcher

import (
	"log"
)

// no using..
func StartFetcher(input <-chan *FetchRequest, output chan<- *FetchResponse) {

	for request := range input {
		// Perform fetch request
		fetchResponse, err := Fetch(request)
		if err != nil {
			log.Fatalf("performing fetch: %s", err)
			continue
		}
		output <- fetchResponse
	}
}
