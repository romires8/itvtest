package fetcher

import (
	"bytes"
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
	"strings"
)

// Fetch request
func Fetch(request *FetchRequest) (*FetchResponse, error) {
	response := &FetchResponse{}
	id, err := uuid.NewRandom()
	if err != nil {
		return nil, fmt.Errorf("generate id error: %s", err)
	}
	response.Id = id.String()

	httpClient := &http.Client{}

	// Add body
	buffer := new(bytes.Buffer)
	if len(request.Body) > 0 {
		buffer.Write(request.Body)
	}

	req, err := http.NewRequest(strings.ToUpper(request.Method), request.Url, buffer)
	if err != nil {
		return nil, fmt.Errorf("creating http request: %s", err)
	}

	// Add headers
	if len(request.Headers) > 0 {
		for key, headers := range request.Headers {
			for _, header := range headers {
				req.Header.Add(key, header)
			}
		}
	}

	httpResponse, err := httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("executing http request: %s", err)
	}
	defer httpResponse.Body.Close()

	respContent, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return nil, fmt.Errorf("reading http response: %s", err)
	}

	response.HttpStatusCode = httpResponse.StatusCode
	response.ContentLength = httpResponse.ContentLength
	response.Header = httpResponse.Header
	response.Body = respContent

	return response, nil
}
