package fetcher

type FetchRequest struct {
	Url     string              `json:"url"`
	Method  string              `json:"method"`
	Headers map[string][]string `json:"headers"`
	Cookies []string            `json:"cookies"`
	Body    []byte              `json:"body"`
}

type FetchResponse struct {
	Id             string              `json:"id"`
	HttpStatusCode int                 `json:"status"`
	Header         map[string][]string `json:"header"`
	ContentLength  int64               `json:"length"`
	Body           []byte              `json:"body"`
}
